EESchema Schematic File Version 4
LIBS:PCB TEMP-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole MH1
U 1 1 5C70847C
P 3550 1100
F 0 "MH1" H 3650 1146 50  0000 L CNN
F 1 "MountingHole" H 3650 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 3550 1100 50  0001 C CNN
F 3 "~" H 3550 1100 50  0001 C CNN
	1    3550 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH3
U 1 1 5C70850E
P 5000 1100
F 0 "MH3" H 5100 1146 50  0000 L CNN
F 1 "MountingHole" H 5100 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5000 1100 50  0001 C CNN
F 3 "~" H 5000 1100 50  0001 C CNN
	1    5000 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH4
U 1 1 5C70857C
P 5700 1100
F 0 "MH4" H 5800 1146 50  0000 L CNN
F 1 "MountingHole" H 5800 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 5700 1100 50  0001 C CNN
F 3 "~" H 5700 1100 50  0001 C CNN
	1    5700 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole MH2
U 1 1 5C7085DC
P 4300 1100
F 0 "MH2" H 4400 1146 50  0000 L CNN
F 1 "MountingHole" H 4400 1055 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3mm" H 4300 1100 50  0001 C CNN
F 3 "~" H 4300 1100 50  0001 C CNN
	1    4300 1100
	1    0    0    -1  
$EndComp
$Comp
L SOLAR:PCbus U1
U 1 1 5C713480
P 2100 1650
F 0 "U1" H 3141 1321 50  0000 L CNN
F 1 "PCbus" H 3141 1230 50  0000 L CNN
F 2 "PC BUS:BUS PC104" H 2100 1650 50  0001 C CNN
F 3 "" H 2100 1650 50  0001 C CNN
	1    2100 1650
	1    0    0    -1  
$EndComp
NoConn ~ 3650 2950
NoConn ~ 3650 2850
NoConn ~ 3650 2800
NoConn ~ 3650 2750
NoConn ~ 3650 2650
NoConn ~ 3650 2600
NoConn ~ 3650 2550
NoConn ~ 3650 2000
NoConn ~ 3650 1950
NoConn ~ 3650 1850
NoConn ~ 3650 1750
NoConn ~ 3650 1700
NoConn ~ 3400 3200
NoConn ~ 3350 3200
NoConn ~ 3050 3200
NoConn ~ 3000 3200
NoConn ~ 2950 3200
NoConn ~ 2600 1450
NoConn ~ 2700 1450
NoConn ~ 2850 1450
NoConn ~ 2900 1450
NoConn ~ 3000 1450
NoConn ~ 3050 1450
NoConn ~ 3200 1450
NoConn ~ 3300 1450
NoConn ~ 3400 1450
Wire Wire Line
	3650 1800 3850 1800
Wire Wire Line
	2150 1450 2150 1250
Wire Wire Line
	3350 1250 3350 1450
Wire Wire Line
	3150 1450 3150 1250
Connection ~ 3150 1250
Wire Wire Line
	3150 1250 3350 1250
Wire Wire Line
	2950 1450 2950 1250
Connection ~ 2950 1250
Wire Wire Line
	2950 1250 3150 1250
Wire Wire Line
	2750 1450 2750 1250
Connection ~ 2750 1250
Wire Wire Line
	2750 1250 2950 1250
Wire Wire Line
	2550 1450 2550 1250
Connection ~ 2550 1250
Wire Wire Line
	2550 1250 2750 1250
Wire Wire Line
	2350 1450 2350 1250
Wire Wire Line
	2150 1250 2350 1250
Connection ~ 2350 1250
Wire Wire Line
	2350 1250 2550 1250
Wire Wire Line
	2150 1250 1950 1250
Wire Wire Line
	1950 1250 1950 1400
Connection ~ 2150 1250
$Comp
L power:GND #PWR0101
U 1 1 5C7BAF9B
P 1950 1400
F 0 "#PWR0101" H 1950 1150 50  0001 C CNN
F 1 "GND" H 1955 1227 50  0000 C CNN
F 2 "" H 1950 1400 50  0001 C CNN
F 3 "" H 1950 1400 50  0001 C CNN
	1    1950 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 2100 3850 2100
Wire Wire Line
	3850 2100 3850 1800
Wire Wire Line
	3650 2900 3850 2900
Connection ~ 3850 2100
Wire Wire Line
	3650 2300 3850 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2300 3850 2100
Wire Wire Line
	3650 2500 3850 2500
Connection ~ 3850 2500
Wire Wire Line
	3850 2500 3850 2300
Wire Wire Line
	3650 2700 3850 2700
Wire Wire Line
	3850 2500 3850 2700
Connection ~ 3850 2700
Wire Wire Line
	3850 2700 3850 2900
Wire Wire Line
	2900 3200 2900 3400
Wire Wire Line
	2900 3400 3100 3400
Wire Wire Line
	3850 3400 3850 2900
Connection ~ 3850 2900
Wire Wire Line
	3100 3200 3100 3400
Connection ~ 3100 3400
Wire Wire Line
	3100 3400 3300 3400
Wire Wire Line
	3300 3200 3300 3400
Connection ~ 3300 3400
Wire Wire Line
	3300 3400 3850 3400
$Comp
L power:GND #PWR0102
U 1 1 5C7BC2F5
P 3850 3400
F 0 "#PWR0102" H 3850 3150 50  0001 C CNN
F 1 "GND" H 3855 3227 50  0000 C CNN
F 2 "" H 3850 3400 50  0001 C CNN
F 3 "" H 3850 3400 50  0001 C CNN
	1    3850 3400
	1    0    0    -1  
$EndComp
Connection ~ 3850 3400
Wire Wire Line
	2750 3200 2750 3400
Wire Wire Line
	2750 3400 2800 3400
Connection ~ 2900 3400
Wire Wire Line
	2800 3200 2800 3400
Connection ~ 2800 3400
Wire Wire Line
	2800 3400 2900 3400
Wire Wire Line
	1900 1700 1700 1700
Wire Wire Line
	1700 1700 1700 1750
Wire Wire Line
	1900 2750 1700 2750
Connection ~ 1700 2750
Wire Wire Line
	1700 2750 1700 2950
Wire Wire Line
	1900 2700 1700 2700
Connection ~ 1700 2700
Wire Wire Line
	1700 2700 1700 2750
Wire Wire Line
	1900 2650 1700 2650
Connection ~ 1700 2650
Wire Wire Line
	1700 2650 1700 2700
Wire Wire Line
	1900 2600 1700 2600
Connection ~ 1700 2600
Wire Wire Line
	1700 2600 1700 2650
Wire Wire Line
	1900 1750 1700 1750
Connection ~ 1700 1750
Wire Wire Line
	1700 1750 1700 2600
$Comp
L power:GND #PWR0103
U 1 1 5C7BFE55
P 1700 2950
F 0 "#PWR0103" H 1700 2700 50  0001 C CNN
F 1 "GND" H 1705 2777 50  0000 C CNN
F 2 "" H 1700 2950 50  0001 C CNN
F 3 "" H 1700 2950 50  0001 C CNN
	1    1700 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 3200 2300 3200
Connection ~ 2300 3200
Wire Wire Line
	2300 3200 2350 3200
Connection ~ 2350 3200
Wire Wire Line
	2350 3200 2400 3200
Connection ~ 2400 3200
Wire Wire Line
	2400 3200 2450 3200
Connection ~ 2450 3200
Wire Wire Line
	2450 3200 2500 3200
Connection ~ 2500 3200
Wire Wire Line
	2500 3200 2550 3200
Connection ~ 2550 3200
Wire Wire Line
	2550 3200 2600 3200
Connection ~ 2600 3200
Wire Wire Line
	2600 3200 2650 3200
Connection ~ 2650 3200
Wire Wire Line
	2650 3200 2700 3200
Wire Wire Line
	2500 3200 2500 3400
Text Label 2500 3400 0    30   ~ 0
5V
Wire Wire Line
	1900 2550 1900 2500
Connection ~ 1900 2150
Wire Wire Line
	1900 2150 1900 2100
Connection ~ 1900 2200
Wire Wire Line
	1900 2200 1900 2150
Connection ~ 1900 2250
Wire Wire Line
	1900 2250 1900 2200
Connection ~ 1900 2300
Wire Wire Line
	1900 2300 1900 2250
Connection ~ 1900 2350
Wire Wire Line
	1900 2350 1900 2300
Connection ~ 1900 2400
Wire Wire Line
	1900 2400 1900 2350
Connection ~ 1900 2450
Wire Wire Line
	1900 2450 1900 2400
Connection ~ 1900 2500
Wire Wire Line
	1900 2500 1900 2450
Wire Wire Line
	1900 2250 1850 2250
Text Label 1850 2250 2    50   ~ 0
5V
Wire Wire Line
	1900 2800 1900 2850
Wire Wire Line
	1900 3200 2150 3200
Connection ~ 1900 2850
Wire Wire Line
	1900 2850 1900 2900
Connection ~ 1900 2900
Wire Wire Line
	1900 2900 1900 2950
Connection ~ 1900 2950
Wire Wire Line
	1900 2950 1900 3200
Connection ~ 2150 3200
Wire Wire Line
	2150 3200 2200 3200
Wire Wire Line
	1900 3200 1900 3400
Connection ~ 1900 3200
Wire Wire Line
	1900 1800 1900 1850
Connection ~ 1900 1850
Wire Wire Line
	1900 1850 1900 1900
Connection ~ 1900 1900
Wire Wire Line
	1900 1900 1900 1950
Connection ~ 1900 1950
Wire Wire Line
	1900 1950 1900 2000
Connection ~ 1900 2000
Wire Wire Line
	1900 2000 1900 2050
Wire Wire Line
	1900 1900 1850 1900
Text Label 1850 1900 2    50   ~ 0
3V3
Text Label 1900 3400 2    30   ~ 0
3V3
Text Label 2200 1450 1    30   ~ 0
5V
Text Label 3650 2050 0    30   ~ 0
3V3
Text Label 3650 1900 0    30   ~ 0
5V
Text Label 2850 3200 3    30   ~ 0
3V3
Text Label 2650 1450 1    30   ~ 0
5V
Text Label 3250 1450 1    30   ~ 0
5V
Text Label 3100 1450 1    30   ~ 0
3V3
Text Label 2800 1450 1    30   ~ 0
3V3
Text Label 2250 1450 1    30   ~ 0
OBC1
Text Label 2300 1450 1    30   ~ 0
OBC2
Text Label 2400 1450 1    30   ~ 0
OBC3
Text Label 2450 1450 1    30   ~ 0
OBC4
Text Label 2500 1450 1    30   ~ 0
OBC5
Text Label 3650 2150 0    30   ~ 0
OBC6
Text Label 3650 2200 0    30   ~ 0
OBC7
Text Label 3650 2250 0    30   ~ 0
OBC8
Text Label 3650 2350 0    30   ~ 0
POW1
Text Label 3650 2400 0    30   ~ 0
POW2
Text Label 3650 2450 0    30   ~ 0
POW3
Text Label 3150 3200 3    30   ~ 0
POW6
Text Label 3200 3200 3    30   ~ 0
POW5
Text Label 3250 3200 3    30   ~ 0
POW4
$EndSCHEMATC
